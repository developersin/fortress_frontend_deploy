import { Injectable } from '@angular/core';
import { HttpRestClient } from 'app/core/http.client';

@Injectable()
export class LoginService {

  private entity = 'login';

  constructor(
    private _httpClient: HttpRestClient
  ) { }

  create(data: any) {
    return this._httpClient.postData(this.entity, data);
  }

  read(id: number) {
    return this._httpClient.getData(this.entity + '/' + id);
  }

  update(id: number, data: any) {
    return this._httpClient.updateData(this.entity + '/' + id, data);
  }

  delete(id: number) {
    return this._httpClient.deleteData(this.entity + '/' + id);
  }

}
