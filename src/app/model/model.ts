export class CurrentUser {

  public id: String;
  public name: String;
  public username: String;
  public authorities: any[];
  constructor() { }

}
