import { Injectable } from '@angular/core';
import { Inject } from '@angular/core';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
// import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Response } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import { HttpParams  } from '@angular/common/http';

import { Observable } from 'rxjs';
import { Subject } from 'rxjs';
import { throwError } from 'rxjs';
import { map } from 'rxjs/operators';
import { catchError } from 'rxjs/operators';


import { SessionHelper } from 'app/core/session.helper';
import { GlobalService } from 'app/core/global';
import { ResponseContentType } from '@angular/http';

@Injectable()
export class HttpRestClient {

  constructor(
    private http: HttpClient,
    private sessionHelper: SessionHelper,
    private router: Router,
    private globalService: GlobalService) {
    this.apiEndpoint = this.globalService.API_END_POINT;
  }

  private apiEndpoint: String;

  createAuthorizationHeader(headers: HttpHeaders) {
    headers.append('Content-Type', 'application/json');

    // if (this.sessionHelper.get("token") != null) {
    //   headers.append('Authorization', 'Bearer ' + this.sessionHelper.get("token"));
    // }
  }

  createBasicAuthorizationHeader(headers: HttpHeaders, username: string, password: string) {
    headers.append('Authorization', 'Basic ' + btoa(username + ':' + password));
  }


  getApiUrl(route: String): string {
    return this.apiEndpoint + '' + route;
  }

  getData(url: String) {

    const authHeaders = new HttpHeaders();
    this.createAuthorizationHeader(authHeaders);
    this.showSpinner();
    // console.log('From getData Authorization : ' + authHeaders.get('Authorization'));
    return this.http.get(this.getApiUrl(url), {
      headers: authHeaders,
      withCredentials: true
    }).pipe(map(res => {
      this.hideSpinner();

      // CHECK THE ERROR TYPE
      this.processHttpCode(res);
      return res;
    }), catchError(res => {
      this.hideSpinner();
      this.processHttpCode(res);
      return throwError(res);
    }));
  }

  search(url: String, params: HttpParams ) {
    const authHeaders = new HttpHeaders();
    this.createAuthorizationHeader(authHeaders);
    this.showSpinner();
    // console.log('From getData Authorization : ' + authHeaders.get('Authorization'));
    return this.http.get(this.getApiUrl(url), {
      headers: authHeaders,
      withCredentials: true,
      params: params
    }).pipe(map(res => {
      this.hideSpinner();
      this.processHttpCode(res);
      return res;
    }), catchError(res => {
      this.hideSpinner();
      this.processHttpCode(res);
      return Observable.throw(res.json());
    }));
  }

  postData(url: String, data: Object) {
    const authHeaders = new HttpHeaders();
    this.createAuthorizationHeader(authHeaders);
    this.showSpinner();
    return this.http.post(this.getApiUrl(url), data, {
      headers: authHeaders,
      withCredentials: true
    }).pipe(map(res => {
      this.hideSpinner();
      this.processHttpCode(res);
      return res;
    }), catchError(res => {
      this.hideSpinner();
      this.processHttpCode(res);
      return throwError(res);
    }));
  }

  updateData(url: String, data: Object) {
    const authHeaders = new HttpHeaders();
    this.createAuthorizationHeader(authHeaders);
    this.showSpinner();
    return this.http.put(this.getApiUrl(url), data, {
      headers: authHeaders,
      withCredentials: true
    }).pipe(map(res => {
      this.hideSpinner();
      this.processHttpCode(res);
      return res;
    }), catchError(res => {
      this.hideSpinner();
      this.processHttpCode(res);
      return throwError(res);
    }));
  }

  patchData(url: String, data: Object) {
    const authHeaders = new HttpHeaders();
    this.createAuthorizationHeader(authHeaders);
    this.showSpinner();
    return this.http.patch(this.getApiUrl(url), data, {
      headers: authHeaders,
      withCredentials: true
    }).pipe(map(res => {
      this.hideSpinner();
      this.processHttpCode(res);
      return res;
    }), catchError(res => {
      this.hideSpinner();
      this.processHttpCode(res);
      return Observable.throw(res.json());
    }));
  }

  deleteData(url: String) {
    const authHeaders = new HttpHeaders();
    this.createAuthorizationHeader(authHeaders);
    this.showSpinner();
    return this.http.delete(this.getApiUrl(url), {
      headers: authHeaders,
      withCredentials: true
    }).pipe(map(res => {
      this.hideSpinner();
      this.processHttpCode(res);
      return res;
    }), catchError(res => {
      this.hideSpinner();
      this.processHttpCode(res);
      return Observable.throw(res.json());
    }));
  }

  refreshSession(url: string) {
    const authHeaders = new HttpHeaders();
    this.createAuthorizationHeader(authHeaders);
    this.showSpinner();

    return this.http.get(this.getApiUrl(url), {
      headers: authHeaders,
      withCredentials: true
    })
      .subscribe(
        (res) => {
          this.hideSpinner();
          this.processHttpCode(res);
        });
  }


  /**
   * spinner methods
   */
  showSpinner() {
    // this.loaderService.increment();
  }

  /**
   * spinner methods
   */
  hideSpinner() {
    // this.loaderService.decrement();
  }

  processHttpCode(response: any) {

    if (response.status === 200) {
      if (response._body.reloadContext === true) {
        // this.sessionHelper.setUser(response._body.currentUser);
      }
    }

    if (response.status === 0) {
      // this.toastService.showAppLevelError('Unable to reach the server');
    }

    if (response.status === 401) {

      // this.sessionHelper.setUser(null);
      this.router.navigate(['/login', { timeout: true }]);
    }

    if (response.status === 403) {

      this.router.navigate(['/authorization-error']);
    }

  }

}
