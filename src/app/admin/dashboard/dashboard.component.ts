import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UiDashboard } from './dashboard.data';
import { DashboardService } from './dashboard.service';
import { SessionHelper } from 'app/core/session.helper';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [DashboardService]
})
export class DashboardComponent implements OnInit {

  uiMode = 'READ';
  dashboard: UiDashboard;
  registerForm: FormGroup;

  constructor(
    private router: Router,
    private dashboardService: DashboardService,
    private sessionHelper: SessionHelper,
    private formBuilder: FormBuilder
  ) {
    this.dashboard = new UiDashboard();
    this.get();
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      regularContributionBelowCeiling: ['', [Validators.required, Validators.min(0), Validators.minLength(1), Validators.maxLength(10)]],
      regularContributionAboveCeiling: ['', [Validators.required, Validators.min(0), Validators.minLength(1), Validators.maxLength(10)]],
      avcContributionBelowCeiling: ['', [Validators.required, Validators.minLength(1), Validators.min(0), Validators.maxLength(10)]],
      avcContributionAboveCeiling: ['', [Validators.required, Validators.min(0), Validators.minLength(1), Validators.maxLength(10)]],
      nisCeiling: ['', [Validators.required, Validators.min(0), Validators.minLength(1), Validators.maxLength(10)]],
      mandatoryRetirementAge: ['', [Validators.required, Validators.min(0), Validators.minLength(1), Validators.maxLength(10)]],
      assumedWorkingAge: ['', [Validators.required, Validators.minLength(1), Validators.min(0), Validators.maxLength(10)]],
      aggressive: ['', [Validators.required, Validators.min(0), Validators.minLength(1), Validators.maxLength(10)]],
      conservative: ['', [Validators.required, Validators.min(0), Validators.minLength(1), Validators.maxLength(10)]]
    });
  }

  save() {
    this.dashboardService.update( 1, this.dashboard).subscribe( (httpResponse: any) => {
      this.dashboard = httpResponse.object;
    });
  }

  get() {
    this.dashboardService.read( 1 ).subscribe( (httpResponse: any) => {
      this.dashboard = httpResponse.object;
    });
  }

  logout() {
    this.sessionHelper.setUser(null);
    this.router.navigate(['/welcome']);
  }

  cancel() {
    this.get();
    this.uiMode = 'READ';
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

}
