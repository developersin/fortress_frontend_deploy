import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NISUserA } from './step3CalcA.model';
import { NISUserAResult } from './step3CalcA.model';
import { NISCalcAService } from 'app/steps/nisCalcA.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-step3calca',
  templateUrl: './step3CalcA.component.html',
  styleUrls: ['./step3CalcA.component.css']
})
export class Step3CalcAComponent implements OnInit {

  dataModel: NISUserA;
  result: NISUserAResult;
  registerForm: FormGroup;

  stepNumber = 1;
  interestRates: any[] = [0.03, 0.05, 0.07];
  retirementAges: any[] = [60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70];

  constructor(
    private router: Router,
    private nisCalcAService: NISCalcAService,
    private formBuilder: FormBuilder,
    private messageService: MessageService
  ) {
    this.dataModel = new NISUserA();
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      birthdate: ['', [Validators.required, Validators.nullValidator]],
      retirementAge: ['', [Validators.required, Validators.min(0), Validators.minLength(1), Validators.maxLength(4)]],
      currentSavingsInPension: ['', [Validators.required, Validators.min(0), Validators.minLength(1), Validators.maxLength(10)]],
      interestRate: ['', [Validators.required, Validators.minLength(1), Validators.min(0), Validators.maxLength(10)]],
      expectedAccumulationByRetirement: ['', [Validators.required, Validators.min(0), Validators.minLength(1), Validators.maxLength(10)]]
    });
  }

  calculate() {
    if (this.registerForm.invalid) {
      return;
    }

    this.nisCalcAService.create(this.dataModel).subscribe((httpResponse: any) => {
      if (httpResponse.success === true) {
        this.result = httpResponse.object;
        this.changeToNextStep();
      } else {
        this.messageService.add({ severity: 'error', summary: 'Failure', detail: 'Incorrect details please enter valid details' });
      }
    }, (error) => {
      this.messageService.add({ severity: 'error', summary: 'Failure', detail: 'Incorrect details please enter valid details' });
    });
  }

  changeToNextStep() {
    if (this.stepNumber < 2) {
      this.stepNumber += 1;
    }
  }

  changeToPreviousStep() {
    if (this.stepNumber > 1) {
      this.stepNumber -= 1;
    }
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }
}
