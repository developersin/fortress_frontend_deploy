export class NISUserA {

  birthdate;
  retirementAge;
  currentSavingsInPension;
  interestRate;
  expectedAccumulationByRetirement;

  constructor() {
    this.birthdate = new Date();
    this.retirementAge = 70;
    this.interestRate = 0.03;
    this.currentSavingsInPension = 10000;
    this.expectedAccumulationByRetirement = 1500000;
  }
}

export class NISUserAResult {

  monthlyInvestmentRequiredToAchieveTarget: any = 0;

  constructor() {
  }
}
