export class NISUserB {

  birthdate;
  retirementAge;
  currentSavingsInPension;
  interestRate;
  monthlySavings;

  constructor() {
    this.birthdate = new Date();
    this.retirementAge = 70;
    this.monthlySavings = 1500;
    this.interestRate = 0.03;
    this.currentSavingsInPension = 10000;
  }
}

export class NISUserBResult {

  totalEstimatedAccumulatedPension: any = 0;

  constructor() {
  }
}
